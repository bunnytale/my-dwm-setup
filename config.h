/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

#define OPAQUE 0xffU

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const unsigned int gappih    = 20;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "Anonymous Pro:style=Italic:size=8:antialias=false:autohint=false", "monospace:size=10" };

static const unsigned int baralpha = 0x47;
static const unsigned int borderalpha = OPAQUE;

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { "#F0F0FF", "#0f0d28", "#0f0d28" },
	[SchemeSel]  = { "#FFFFFF", "#8813db", "#8813db" },
	[SchemeStatus]  = { "#F0F0FF", "#0f0d28",  "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { "#FFFFFF", "#8813db",  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
    [SchemeTagsNorm]  = { "#F0F0FF", "#0f0d28",  "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
    [SchemeInfoSel]  = { "#F0F0FF", "#0f0d28",  "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
    [SchemeInfoNorm]  = { "#0f0d28", "#0f0d28",  "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title            tags mask     isfloating   monitor    float x,y,w,h        floatborderpx*/
	{ NULL,       NULL,   "scratchpad",        0,            1,           -1,        400, 200, 500, 400,    's' },
	{ NULL,       NULL,   "helper",            0,            1,           -1,         50,  50, 500, 200,    'a' },
    { NULL,       NULL,   "running processes", 0,            1,           -1,        200, 400, 500, 200,    'm' },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "h[]",      deck },
	{ "ttt",      bstack },
	{ "===",      bstackhoriz },
	{ "hhh",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|m|",      centeredmaster },
	{ ">m>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL},
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \
    { Mod3Mask|Mod4Mask|ShiftMask,            KEY,      swaptags,       {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon};
static const char *termcmd[]  = { "st", NULL };

#include "movestack.c"
/*First arg only serves to match against key in rules*/
static const char *scratchpadcmd[] = {"s", "st", "-t", "scratchpad", NULL}; 
static const char *processmngcmd[] = {"m", "st", "-t", "running processes" , "-e", "top"};
static const char *helpercmd[] = {"a", "st", "-t", "helper", NULL}; 

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,       spawn,          {.v = dmenucmd } },
	{ MODKEY,	                    XK_Return,  spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_a,       togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY|ShiftMask,             XK_Return,  togglescratch,  {.v = helpercmd } },
    { MODKEY|ShiftMask,             XK_m,       togglescratch,  {.v = processmngcmd } },

	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	
	{ MODKEY|ShiftMask,             XK_s,      zoom,           {0} },

	{ MODKEY|Mod4Mask,              XK_z,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_z,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_v,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_v,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask|ControlMask,  XK_v,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask|ControlMask, XK_v,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod3Mask,              XK_v,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod3Mask|ShiftMask,    XK_v,      incrihgaps,     {.i = -1 } },
	/* { MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } }, */
	{ MODKEY|Mod4Mask,              XK_a,      togglegaps,     {0} },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_x,      defaultgaps,    {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	
	{ MODKEY|ShiftMask,              XK_c,      spawn,      SHCMD("slock") },
	{ MODKEY|ShiftMask,              XK_c,      spawn,      SHCMD("cmus-remote -s >/dev/null 2>&1") },

	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,		        XK_t,	   setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_g,      setlayout,      {.v = &layouts[9]} },
	{ MODKEY|ShiftMask,		        XK_w,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_w,      setlayout,      {.v = &layouts[10]} },
	{ MODKEY,                       XK_y,      setlayout,	   {.v = &layouts[12]} },
	{ MODKEY|ShiftMask,             XK_y,      setlayout,	   {.v = &layouts[11]} },
	{ MODKEY|ShiftMask,             XK_u,      setlayout,	   {.v = &layouts[13]} },

	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },

	{ MODKEY,                       XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY,                       XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY,                       XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY,                       XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ShiftMask,             XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ShiftMask,             XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ShiftMask,             XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ShiftMask,             XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },
	
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
    { MODKEY,                       XK_r,      zoom,           {0}},
    { MODKEY,                       XK_slash,  focusmaster,           {0}},
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      try_quit,       {0} },
	{ MODKEY|ShiftMask|ControlMask, XK_q,      quit,           {0} },
    
	{ MODKEY|ShiftMask,             XK_equal, spawn,  SHCMD("cmus-remote -v +4")},
	{ MODKEY|ShiftMask,             XK_minus, spawn,  SHCMD("cmus-remote -v -4")},

        { 0,                            XF86XK_AudioPrev,        spawn,  SHCMD("cmus-remote --prev 2>/dev/null")},
        { 0,                            XF86XK_AudioNext,        spawn,  SHCMD("cmus-remote --next 2>/dev/null")},
        { 0,                            XF86XK_AudioPlay,        spawn,  SHCMD("cmus-remote -u 2>/dev/null")},
        { 0,                            XF86XK_AudioPause,       spawn,  SHCMD("cmus-remote -u 2>/dev/null")},
        { 0,                            XF86XK_AudioStop,        spawn,  SHCMD("cmus-remote -s 2>/dev/null")},
        { 0,                            XF86XK_AudioRewind,      spawn,  SHCMD("cmus-remote --seek -10 2>/dev/null")},
        { 0,                            XF86XK_AudioForward,     spawn,  SHCMD("cmus-remote --seek +10 2>/dev/null")},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

/* how many windows should be open when quitting? */
/* on a stock dwm install, this seems to be two; however, you'll have to
 * change it depending on how many invisible x windows exist */
/* you can get a list with `xwininfo -tree -root`. */
static const int EMPTY_WINDOW_COUNT = 2;
